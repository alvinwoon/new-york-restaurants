var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var query = connection.query('select * from restaurant');
	query
	  .on('error', function(err) {
	    // Handle error, an 'end' event will be emitted after this as well
	  })
	  .on('fields', function(fields) {
	    // the field packets for the rows to follow
	  })
	  .on('result', function(row) {
	    // Pausing the connnection is useful if your processing involves I/O
	    connection.pause();

	    processRow(row, function() {
	    	console.log(row);
	      connection.resume();
	    });
	  })
	  .on('end', function() {
	    // all rows have been received
	  });

  res.render('index', { title: 'Express' });
});

module.exports = router;
