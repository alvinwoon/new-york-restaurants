//default is NYC
var TLDR_PLACE = {
	defaultRadius: {
    	stops: [[8, 1], [11, 3], [16, 10]]
	},
	coordinates: [-73.9299201965332, 40.74699686227001],
	restaurantsQuery: 'https://d2p48cw50pigrx.cloudfront.net/ny.json',
	//restaurantsQuery: 'https://d2p48cw50pigrx.cloudfront.net/restaurants_v7.json.gz',
	//restaurantsQuery: '/json/restaurants/ny',
	restaurantCount: 25323,
	inspectionCount: 468932,
	customFilterContent: [
		{
			id: 'customRats',
			title: 'Restaurants with rats issue',
			url: '/json/customNycFilter/rats',
			circle:{
				color: '#E32756'
			}
		},
		{
			id: 'customBest',
			title: 'A+ restaurants',
			url: '/json/customNycFilter/best',
			circle:{
				color: '#54E809'
			}
		},
		{
			id: 'customWorst',
			title: 'Worst of the worst',
			url: '/json/customNycFilter/worst',
			circle:{
				color: '#E8AD09'
			}
		},
		{
			id: 'customClose',
			title: 'Establishment closed by Health Department <span style="font-size:14px">(some reopened)</span>',
			url: '/json/customNycFilter/closed',
			circle:{
				color: '#D54CED'
			}
		}
	]
};

if(__PLACE === 'sf'){
	TLDR_PLACE.coordinates = [-122.431297, 37.773972];
	TLDR_PLACE.restaurantsQuery = 'https://d2p48cw50pigrx.cloudfront.net/sf_v1.json.gz';
	TLDR_PLACE.restaurantCount = 7519;
	TLDR_PLACE.inspectionCount = 27346;
	TLDR_PLACE.customFilterContent = [
		{
			id: 'customVermin',
			title: 'Restaurants with high risk vermin infestation',
			url: '/json/customSfFilter/vermin',
			circle:{
				color: '#E32756'
			}
		},
		{
			id: 'customWiping',
			title: 'Unclean wiping clothes',
			url: '/json/customSfFilter/wiping',
			circle:{
				color: '#D54CED'
			}
		},
		{
			id: 'customLatest',
			title: 'Most recent 50 inspections',
			url: '/json/customSfFilter/latest',
			circle:{
				color: '#E8AD09'
			}
		}
	]
}

var TLDR = {
	tooltip: null,
	markers: null,
	map: null,
	categoriesColors: null,
	gradeColors:null,
	popup: null,
	defaultMapType: 'cuisine',
	customFilterMarkerSize: TLDR_PLACE.defaultRadius,
	
	createInitialMarkers: function(data){
		var categories = {};
		$.each(data, function( key, val ) {
			var obj = {
			  "type": "Feature",
			  "geometry": {
			    "type": "Point",
			    "coordinates": [val.longitude, val.latitude]
			  },
			  "properties": {
			    "title": val.camis
			  }
			}

			if(__PLACE === 'nyc'){

				if(!/[^a-z]/i.test(val.cuisine_description)){
					var cuisine = val.cuisine_description;
					if (categories[cuisine] === undefined){	
						categories[cuisine] = [];
			  		}
			  		categories[cuisine].push(obj);	
				}

		  		if(val.grade !== 'A' &&
		  		   val.grade !== 'B' && 
		  		   val.grade !== 'C' && 
		  		   val.grade !== 'Z'){
		  			return;
		  		}

		  		var grade = val.grade + ' grade';
		  		if (categories[grade] === undefined){	
		  			categories[grade] = [];
		  		}
		  		categories[grade].push(obj);
	  		} else {
	  			var score = parseInt(val.score);
	  			var scoreProperty = '';
	  			if(score >= 90){
	  				scoreProperty = '90+';
	  			} else if(score < 90 && score >= 85){
	  				scoreProperty = '85 - 89';
	  			} else if(score < 85 && score >= 80){
	  				scoreProperty = '80 - 84';
	  			} else if(score < 80 && score >= 75){
	  				scoreProperty = '75 - 79';
	  			} else if(score < 75 && score >= 70){
	  				scoreProperty = '70 - 74';
	  			} else if(score < 70 && score >= 65){
	  				scoreProperty = '65 - 69';
	  			} else if(score < 65 && score >= 60){
	  				scoreProperty = '60 - 64';
	  			} else if(score < 60 && score >= 55){
	  				scoreProperty = '55 - 59';
	  			} else {
	  				scoreProperty = 'Less than 55';
	  			}

	  			if (categories[scoreProperty] === undefined){	
		  			categories[scoreProperty] = [];
		  		}
		  		categories[scoreProperty].push(obj);
	  		}
		});
		TLDR.markers = categories;
	},

	request: function(callback){
		if(TLDR.markers){
			if(callback){
				callback();
			}
			return;
		}

		fetch(TLDR_PLACE.restaurantsQuery, {
			headers: {
			    'Accept-Encoding': 'gzip'
			}
		}).then(function(response) { 
			return response.json();
		}).then(function(data) {
			TLDR.createInitialMarkers(data);
			if(callback){
				callback();
			}
		});
	},

	hideAllLayers: function(radius){
		_.each(TLDR.markers, function(category, key){
			if(key === ' grade' || key.indexOf('Graded') > -1){
				return;
			}
			var ids = _.kebabCase(key);
			TLDR.map.setLayoutProperty(ids, 'visibility', 'none');	
			TLDR.map.setPaintProperty(ids, 'circle-radius', radius || TLDR_PLACE.defaultRadius);
		});
	},

	showLayer: function(id, radius){
		TLDR.map.setLayoutProperty(id, 'visibility', 'visible');	
		TLDR.map.setPaintProperty(id, 'circle-radius', radius || TLDR_PLACE.defaultRadius);	
	},

	generateFeatures: function(data, id){
		var arr = [];
		_.each(data, function(val){
			var feature = {
			  "type": "Feature",
			  "geometry": {
			    "type": "Point",
			    "coordinates": [val.longitude, val.latitude]
			  },
			  "properties": {
			    "title": val.camis
			  }
			}
			arr.push(feature);
		});

		TLDR.markers[id] = arr;
		var features = {
		    "type": "FeatureCollection",
		    "features": arr
		};
		TLDR.map.addSource(_.camelCase(id), {
			type: 'geojson',
			data: features 
		});
	},

	reset: function(id, loading){
		TLDR.map.off('click');
		TLDR.map.off('hover');
		//var arr = _.isArray(id) ? id : [_.kebabCase(id)]; 
		//TLDR.addMarkersEvent(arr);
		if(loading){
			loading.hide();
		}
	},

	menuInit: function(){
		var menu = $('<div id="menu" />');

		var placeNavigator = $('<div id="pn" />');
		var nycTab = $('<p data-url="nyc">New York City</p>');
		var sfTab = $('<p data-url="sf">San Francisco</p>');
		placeNavigator.append(nycTab, sfTab);

		if(__PLACE === 'nyc'){
			nycTab.addClass('pn-s');
		} else {
			sfTab.addClass('pn-s');
		}

		placeNavigator.on('click', 'p', function(){
			window.location.href = '/'+$(this).attr('data-url');
		});

		/*
		var tabs =  $('<ol id="tabs" />');
		var cuisineTab = $('<li id="tab-cuisine" class="tab-selected">Filter by Cuisine</li>');
		var gradeTab = $('<li id="tab-grade">Grade</li>');
		tabs.on('click', 'li', function(){
			$('.tab-selected').removeClass();
			$(this).addClass('tab-selected');

			TLDR.hideAllLayers();
			if($(this).attr('id') === 'tab-cuisine'){

			} else {
				TLDR.request();
			}

		});
		tabs.append(cuisineTab, gradeTab);*/

		var cuisine = $('<div id="cuisine" />');
		var i = 0;
		var allText = __PLACE === 'sf' ? 'All scores' : 'All';
		var all = $('<p><span class="c_color" style="background:white"></span><span class="c_cat">'+allText+'</span><i>20k+ restaurants</i></p>');
		cuisine.append(all);

		_.each(TLDR.markers, function(item, key){
			if(key.indexOf('grade') > -1){
				if(key === ' grade' || key.indexOf('Graded') > -1 || __PLACE === 'sf'){
					return;
				}
				var el = $('<p class="gradeStyle"><span class="c_color" style="background:#'+TLDR.gradeColors[i]+'"></span><span class="c_cat">'+key+'</span><i>'+item.length+' restaurants</i></p>');				
				all.after(el);	
			} else {
				var el = $('<p><span class="c_color" style="background:#'+TLDR.categoriesColors[i]+'"></span><span class="c_cat">'+key+'</span><i>'+item.length+' restaurants</i></p>');
				cuisine.append(el);	
			}
			i++;
		});

		cuisine.on('click', 'p', function(){
			var id = _.kebabCase($(this).find('.c_cat:eq(0)').text());
			
			if(TLDR.popup){
				TLDR.popup.remove();
			}

			TLDR.hideAllLayers();

			if(id === 'all' || id === 'all-scores'){
				var arr = [];	
				_.each(TLDR.markers, function(category, key){
					if(key === ' grade' || key.indexOf('Graded') > -1 || key.indexOf('custom') > -1){
						return;
					}
					var ids = _.kebabCase(key);
					arr.push(ids);
					TLDR.showLayer(ids, 3);
				})
				TLDR.reset(arr);
			} else if(id.indexOf('grade') > -1) {
				TLDR.showLayer(id, id === 'a-grade' ? 3 : 6);
				TLDR.reset(id);
			} else {
				TLDR.showLayer(id, __PLACE === 'nyc' ? 10 : 6);
				TLDR.reset(id);
			}
			
		});

		var customFilter = $('<div id="cusFil" />');
		_.each(TLDR_PLACE.customFilterContent, function(item){
			var loading = $('<div class="customLoading">searching...</div>');
			var el = $('<h3>'+item.title+'</h3>');
			el.append(loading);
			var id = item.id;
			el.on('click', function(){
				loading.show();
				TLDR.hideAllLayers(TLDR.customFilterMarkerSize);
				if(TLDR.popup){
					TLDR.popup.remove();
				}
				if(!_.isUndefined(TLDR.markers[id])){
					TLDR.showLayer(_.kebabCase(id), 6);
					TLDR.reset(id, loading);
					return false;
				}

				//fetch if data does not exist
				fetch(item.url).then(function(response) { 
					return response.json();
				}).then(function(data) {
					TLDR.generateFeatures(data, id);
					TLDR.map.addLayer({
					    "id": _.kebabCase(id),
					    "type": "circle",
					    "source": _.camelCase(id),
					    "paint": {
					        "circle-radius": 6,
					        "circle-opacity": 0.6,
					        "circle-blur": 0.1,
					        "circle-color": item.circle.color
					    }
					});
					TLDR.reset(id, loading);
					var arr = _.isArray(id) ? id : [_.kebabCase(id)]; 
					TLDR.addMarkersEvent(arr);
				});
			})
			customFilter.append(el);
		});

		var search = $('<div id="search"></div>');
		var loading = $('<div class="customLoading">searching...</div>');
		var searchForm = $('<input type="text" name="searchField" placeholder="Search for restaurants" id="searchField" />')
		searchForm.on("keypress", function(e) {
            /* ENTER PRESSED*/
            if (e.keyCode == 13) {
                var _val = $(this).val();
                var id = 'customSearch';
                if(_val === ''){
                	return;
                }

                loading.show();
                TLDR.hideAllLayers();
                //return;
				if(TLDR.popup){
					TLDR.popup.remove();
				}

                fetch('/json/search/'+__PLACE+'/'+_val).then(function(response) { 
					return response.json();
				}).then(function(data) {
					if(!_.isUndefined(TLDR.markers[id])){
						delete TLDR.markers[id];
						TLDR.map.removeSource(_.camelCase(id));
						TLDR.map.removeLayer(_.kebabCase(id));
					}

					var features = TLDR.generateFeatures(data, id);
					var color = _.shuffle(TLDR.categoriesColors);
					TLDR.map.addLayer({
					    "id": _.kebabCase(id),
					    "type": "circle",
					    "source": _.camelCase(id),
					    "paint": {
					        "circle-radius": 6,
					        "circle-opacity": 0.6,
					        "circle-blur": 0.1,
					        "circle-color": "#"+color[0]
					    }
					});
					TLDR.reset(id, loading);
					var arr = _.isArray(id) ? id : [_.kebabCase(id)]; 
					TLDR.addMarkersEvent(arr);
				});
                return false;
            }
        });
		
		search.append(searchForm, loading);

		var footer = $('#overlay');
		var footerText = $('<h1 id="footerHeader">Safediningnyc.com</h1>');
		var footerInfo = $('<div>'+
			'<p><strong>Safediningnyc.com</strong> is a real-time project that maps New York City and San Francisco restaurants inspection reports from Department of Health. Unannounced sanitary inspections of every restaurant are performed at least once per year. Violation points, letter grade and violation descriptions can be explored in the map. </p>'+
			'<p><a href="http://www1.nyc.gov/assets/doh/downloads/pdf/rii/how-we-score-grade.pdf" target="_blank">NYC scoring and grading mechanism</a></p>'+
			'<p><a href="https://www.sfdph.org/dph/EH/Food/Score/" target="_blank">SF scoring and grading mechanism</a></p>'+
			'<p class="company">A public service from <a href="https://puppylab.com" target="_blank">Puppylab</a></p>'+
			'<div class="others"><p><a href="https://terrordb.com" target="_blank">Terrordb.com</a><span>Analysis of terror activities from 1979 - 2015</span></p><p><a href="https://theclintondossier.com" target="_blank">theclintondossier.com</a><span>Visualising Clinton\'s dossiers</span></p><p><a href="https://csvchart.io" target="_blank">csvchart.io</a><span>Convert csv into beautiful charts</span></p></div>'+
			'</div>');
		var close = $('<span id="o_close">Close</span>');
		footer.append(footerText, footerInfo, close);

		close.on('click', function(){
			footer.hide();
		});

		var foot = $('<div id="footer"><p>About Safediningnyc.com</p></div>');
		foot.on('click', function(){
			footer.show();
		});

		menu.append(placeNavigator, customFilter, search, cuisine, foot);

		var mobileButton = $('<a id="mobileButton" href="javascript:void(0)">Menu</a>');
		mobileButton.on('click', function(){
			if(TLDR.popup){
				TLDR.popup.remove();
			}
			menu.toggle();
		})
		$('body').append(menu, mobileButton);
	},

	addMarkersEvent: function(ids){
		TLDR.map.on('mousemove', function (e) {
			var features = TLDR.map.queryRenderedFeatures(e.point, {
	            layers: ids
	        });
	        if(features.length){
	        	//console.log(features);
	        	TLDR.map.getCanvas().style.cursor = 'pointer';
		    }
		});

		TLDR.map.on('click', function (e) {
			if($('#mobileButton').is(':visible')){
				$('#menu').hide();
			}
			
			var features = TLDR.map.queryRenderedFeatures(e.point, {
	            layers: ids
	        });
		    if (!features.length) {
		        return;
		    }
		    var feature = features[0];
		    TLDR.popup = new mapboxgl.Popup()
		        .setLngLat(feature.geometry.coordinates)
		        .setHTML('<div id="markerContent"><p id="markerLoading">loading...</p></div>')
		        .addTo(TLDR.map);
		    TLDR.requestRestaurant(feature);
		});
	},

	addMarkers: function(){
		$('#loading').fadeOut('slow').remove();
		
		//get different colors for each category
		TLDR.categoriesColors = palette('tol-dv', _.size(TLDR.markers));
		TLDR.gradeColors = palette('tol-dv', 8);

		var i = 0;
		
		TLDR.menuInit();

		var layerIDs = [];
		_.each(TLDR.markers, function(category, key){
			var color = TLDR.categoriesColors[i];
			var visi = 'visible';
			
			if(key.indexOf('grade') > -1){
				if(key === ' grade' || key.indexOf('Graded') > -1){
					return;
				}
				color = TLDR.gradeColors[i];
				visi = 'none';
			}

			var features = {
			    "type": "FeatureCollection",
			    "features": category
			};
			var name = _.camelCase(key);
			var markers = { type: 'geojson', data: features };
			var _id = _.kebabCase(key);

			layerIDs.push(_id);
			TLDR.map.addSource(name, markers);


			TLDR.map.addLayer({
			    "id": _id,
			    "type": "circle",
			    "source": name,
			    "layout":{
			    	"visibility": visi
			    },
			    "paint": {
			        "circle-radius": TLDR_PLACE.defaultRadius,
			        "circle-opacity": 0.6,
			        "circle-blur": 0.1,
			        "circle-color": "#"+color
			    }
			});

			i++;
		});

		TLDR.addMarkersEvent(layerIDs);
	},

	requestRestaurant: function(feature){
		var camis = feature.properties.title;
		fetch('/json/restaurant/'+__PLACE+'/'+ camis).then(function(response) { 
			return response.json();
		}).then(function(data) {
			//console.log(data);
			var el = $('#markerContent');
			var content = $('<div class="markerHeader"></div>');
			var h1 = $('<h1>'+data[0].DBA+'</h1>');
			if(__PLACE === 'sf'){
				var address = $('<h2>'+data[0].street+'</h2>');
			} else {
				var address = $('<h2>'+data[0].BUILDING+ ' ' + _.capitalize(data[0].STREET) + ', ' + _.capitalize(data[0].BORO) + '</h2>');
			}
			if(__PLACE === 'nyc'){
				var disclaimer = $('<p class="lowerDIsclaimer"><a href="http://www1.nyc.gov/assets/doh/downloads/pdf/rii/how-we-score-grade.pdf">Score system: 0 - 28+. Lower the better</a></p>');
			} else {
				var disclaimer = $('<p class="lowerDIsclaimer"><span>Score system: 0 - 100. Higher the better</span></p>');
			}
			var inspection = $('<ol class="markerInspections"></ol>');
			var averageScore = 0;
			_.each(data, function(item, key){
				averageScore = item.SCORE + averageScore;
				var critical = item.CRITICAL_FLAG === 'Critical' || item.CRITICAL_FLAG === 'High Risk' ? ' &middot; <span class="critical">' + item.CRITICAL_FLAG + '</span>' : '';
				var _score = item.SCORE;
				var scoreEl = '';
				if(_score){
					scoreEl = ' &middot; <span class="insScore">Score: ' + _score + '</span>';
				}
				var ins = $('<li><p class="insTime">'+$.timeago(new Date(item.INSPECTION_DATE).toDateString())+ critical + scoreEl + '</p><p class="insDesc">'+item.VIOLATION_DESCRIPTION+'</p></li>');
				inspection.append(ins);
			});

			averageScore = Math.floor(averageScore / data.length);
			var recentGrades = _.map(data, 'GRADE');
			var scoreNum = data[0].SCORE || '-';

			recentGrades = _.filter(recentGrades, function(item){ 
				return item === 'A' || item === 'B' || item === 'C' || item === 'Z'; 
			});
			var grade = recentGrades[0];

			recentGrades = _.slice(recentGrades, 0, 6).join('');

			var score = $('<div class="score '+__PLACE+'"></div>');
			var avgScore = $('<div class="scoreColumn avgScoreStyle"><p class="scoreLabel">Avg score</p><p class="scoreNumber">' + averageScore + '</p><p class="hdi">'+ data.length +' inspections</p></div>');
			var latestScore = $('<div class="scoreColumn"><p class="scoreLabel">Recent score</p><p class="scoreNumber">' + scoreNum + '</p><p class="hdi">'+ $.timeago(data[0].INSPECTION_DATE) +'</p></div>');
			if(__PLACE === 'nyc'){
				var grade = $('<div class="scoreColumn grade g_'+grade+'"><p class="scoreLabel">Grade</p><h1>'+grade+'</h1><p class="hdi">Recent: '+recentGrades+'</p></div>');
			} else {
				var color = feature.layer.paint['circle-color'];
				score.css('background', color);
				var grade = null;
			}
			score.append(latestScore, avgScore, grade, disclaimer);

			content.append(h1, address, score, inspection);
			//console.log(inspection);
			el.empty().append(content);
		});
	},
	loading: function(){
		var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
		$('#num_of_res').animateNumber({ 
			number: TLDR_PLACE.restaurantCount, 
			numberStep: comma_separator_number_step,
			easing: 'easeInQuad' }, 1500, TLDR.initMap);
		$('#num_of_reports').animateNumber({ 
			number: TLDR_PLACE.inspectionCount, 
			numberStep: comma_separator_number_step,
			easing: 'easeInQuad' }, 1500);	
	},
	initMap: function(){
		mapboxgl.accessToken = 'pk.eyJ1IjoibHluZGFqb2huc29uIiwiYSI6ImNpcWo3Nm9ybTA5Zjlmd20xdG51anFjaDQifQ.EdnfEm2X0-D03tlTnH2KEQ';
		TLDR.map = new mapboxgl.Map({
			container: 'map', // container id
		    style: 'mapbox://styles/mapbox/dark-v8', //hosted style id
		    center: TLDR_PLACE.coordinates, // starting position
		    zoom: 11, // starting zoom,
			attributionControl: false
		});
		TLDR.map.on('load', function(data) {
			TLDR.request(TLDR.addMarkers);
		});
	}
}
$(document).ready(function() {
	TLDR.request();
	TLDR.loading();
});

