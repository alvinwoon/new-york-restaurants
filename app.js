var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
//var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
/*var md5 = require('md5');

var redis   = require("redis");
var RedisClient  = redis.createClient({
    host: '54.163.144.79'
});
RedisClient.auth('_ry*qj=#=-zb4VAD');
*/
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

var mysql      = require('mysql');
var connection = mysql.createConnection({
  //host     : '198.199.100.69',
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'nyc_rest_inspection'
});

connection.connect();

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(compression({filter: shouldCompress}))

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }

  // fallback to standard filter function
  return compression.filter(req, res)
}

var queryEngine = function(sql){
  return new Promise(function(resolve, reject) {
    //var sqlHash = md5(sql);
    /*RedisClient.get(sqlHash, function(err, data){
      if(data){
        console.log('Cache hit for ' + sql);
        resolve(JSON.parse(data));  
      } else {*/
        connection.query(sql, function(err, rows, fields) {
          //connection.end();
          if (err) reject(err);
          //RedisClient.set(sqlHash, JSON.stringify(rows), function(err, data){
          resolve(rows);
          //});
        });    
      //}
    //});
  });
}

app.get('/', function(req, res, next) {
  res.render('index', {
    place: 'nyc'
  });
});

app.get('/:place', function(req, res, next) {
  var place = req.params.place;
  
  if(place !== 'nyc' && place !== 'sf'){
    place = 'nyc';
  }

  res.render('index', {
    place: place
  });
});


app.get('/json/restaurants/:place', function(req, res, next){
  var sql = 'select rest.*,  t2.grade, t2.cuisine_description from (select distinct t1.camis, t1.longitude, t1.latitude from restaurant t1 where t1.longitude is not null limit 10) as rest inner join (select * from inspection order by STR_TO_DATE(INSPECTION_DATE, "%m/%d/%Y") desc) t2 using(camis) group by camis order by cuisine_description';
  if(req.params.place === 'sf'){
    sql = 'select t1.id, t1.business_id as camis, t1.longitude, t1.latitude, t2.score from sf_restaurant t1 inner join (select * from sf_inspection order by date desc) t2 using (business_id) where t1.longitude is not null  and t2.score != "" group by business_id order by CONVERT(t2.score, unsigned integer) desc';
  }
  queryEngine(sql).then(function(result) { 
    res.json(result);
  });
});

app.get('/json/search/:place/:query', function(req, res, next){
  var sql = 'select t1.* from restaurant t1 inner join (select * from inspection where dba regexp '+connection.escape(req.params.query)+' group by camis) t2 using(camis)';
  if(req.params.place === 'sf'){
    sql = 'select t1.business_id as camis, t1.latitude, t1.longitude from sf_restaurant t1 where name regexp '+connection.escape(req.params.query);
  }
  queryEngine(sql).then(function(result) { 
    res.json(result);
  });
});


app.get('/json/customNycFilter/:id', function(req, res, next){
  var sql = 'select t1.* from restaurant t1 inner join (select * from inspection where VIOLATION_DESCRIPTION REGEXP " rat | mice " and INSPECTION_DATE REGEXP "2016")  t2 using(camis) group by t1.camis order by count (*) desc limit 300';
  if(req.params.id === 'worst'){
    sql = 'select t1.*, t2.GRADE from restaurant t1 inner join (select * from inspection where INSPECTION_DATE REGEXP "2016" and (grade != "" and grade != "A" and grade != "B"))  t2 using(camis) group by t1.camis order by count(*) desc limit 300';
  } else if(req.params.id === 'best'){
    sql = 'select t1.* from restaurant t1 inner join (select * from inspection where INSPECTION_DATE REGEXP "2016" and grade = "A")  t2 using(camis) group by t1.camis order by count(*) desc limit 300';
  } else if(req.params.id === 'closed'){
    sql = 'select t1.* from restaurant t1 inner join (select * from inspection where action regexp " closed " and INSPECTION_DATE REGEXP "2016" group by camis)  t2 using(camis)';
  }
  queryEngine(sql).then(function(result) { 
    res.json(result);
  });
});

app.get('/json/customSfFilter/:id', function(req, res, next){
  var sql = 'select t1.business_id as camis, t1.latitude, t1.longitude from sf_restaurant t1 inner join (select * from sf_violation where description like "%High risk vermin%" order by date desc) t2 using(business_id) group by camis';
  if(req.params.id === 'wiping'){
    sql = 'select t1.business_id as camis, t1.latitude, t1.longitude from sf_restaurant t1 inner join (select * from sf_violation where description like "Wiping %" order by date desc limit 500) t2 using(business_id) group by camis';
  } else if(req.params.id === 'latest'){
    sql = 'select t1.business_id as camis, t1.latitude, t1.longitude from sf_restaurant t1 inner join (select * from sf_inspection order by date desc limit 50) t2 using(business_id) group by camis';
  }

  queryEngine(sql).then(function(result) { 
    res.json(result);
  });
});

app.get('/json/restaurant/:place/:camis', function(req, res, next){
  var sql = 'select t2.* from restaurant t1 inner join (select * from inspection where camis = '+connection.escape(req.params.camis)+' order by STR_TO_DATE(INSPECTION_DATE, "%m/%d/%Y")) t2 using(camis)';
  if(req.params.place === 'sf'){
    sql = 'select t1.business_id as camis, t1.name as DBA, t1.address as street, t1.latitude, t1.longitude, CONVERT(t2.score, unsigned integer) as SCORE, cast(STR_TO_DATE(t2.date, "%Y%m%d") as date) as INSPECTION_DATE, t2.type, t3.risk_category as CRITICAL_FLAG, t3.description as VIOLATION_DESCRIPTION from sf_restaurant t1 inner join (select * from sf_inspection where business_id = '+connection.escape(req.params.camis)+' order by date desc) t2 using(business_id) inner join sf_violation t3 using(business_id)';  
  }
  queryEngine(sql).then(function(result) { 
    res.json(result);
  });
});

//connection.end();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
